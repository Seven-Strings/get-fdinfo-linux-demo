import os
from datetime import datetime as dt
from pathlib import Path

import PySimpleGUI as sg
from pandas import DataFrame
import pwd

EXIT = False
TESTING = False
SCRIPT_ROOT = os.path.dirname(os.path.realpath(__file__))
COLUMNS = ["Name", "Size(B)", "Size(MB)", "Extension", "CreationTime", "LastAccessTime", "LastWriteTime", "Owner",
           "FullName"]
column_name = COLUMNS[0]
column_size_b = COLUMNS[1]
column_size_mb = COLUMNS[2]
column_extension = COLUMNS[3]
column_ct = COLUMNS[4]
column_lat = COLUMNS[5]
column_lwt = COLUMNS[6]
column_owner = COLUMNS[7]
column_fullname = COLUMNS[8]

y = month = d = h = m = date = file_date = None


def sb_main(path):
    directory = {}
    if TESTING:
        print(answer)
    # if checkbox "Name", default=True
    if answer_checkbox_name:
        directory[column_name] = path.name

    # if checkbox "Size(B)", default=False
    if answer_checkbox_size_b:
        directory[column_size_b] = path.stat().st_size

    # if checkbox "Size(MB)", default=True
    if answer_checkbox_size_mb:
        # if checkbox "Upper level scan"
        if answer_checkbox_basic:
            size_mb = size / 1024 / 1024
            directory[column_size_mb] = round(size_mb, 2)
        elif not answer_checkbox_basic:
            directory[column_size_mb] = round(path.stat().st_size / 1024 / 1024, 2)

    # print(directory)
    # if checkbox "Type", default=False
    if answer_checkbox_extension:
        if path.is_file():
            file_name, file_extension = os.path.splitext(path.absolute())
            if file_extension == '' and '.' not in file_name:
                directory[column_extension] = ''
            elif file_extension == '' and '.' in file_name:
                directory[column_extension] = path.name
            else:
                directory[column_extension] = file_extension
        else:
            directory[column_extension] = "D"

    # if checkbox "CreationTime", default=False
    if answer_checkbox_ct:
        try:
            directory[column_ct] = dt.fromtimestamp(path.stat().st_ctime).isoformat()
        except Exception:
            directory[column_ct] = ''

    # if checkbox "LastAccessTime", default=False
    if answer_checkbox_lat:
        try:
            directory[column_lat] = dt.fromtimestamp(path.stat().st_atime).isoformat()
        except Exception:
            directory[column_lat] = ''

    # if checkbox "LastWriteTime", default=False
    if answer_checkbox_lwt:
        try:
            directory[column_lwt] = dt.fromtimestamp(path.stat().st_mtime).isoformat()
        except Exception:
            directory[column_lwt] = ''

    # if checkbox "Owner", default=False
    if answer_owner:
        try:
            user_id = path.stat().st_uid
            pwd.getpwuid(user_id)

            directory[column_owner] = pwd.getpwuid(user_id).pw_name

        except Exception:
            directory[column_owner] = ''

    directory[column_fullname] = path.absolute()

    files_list.append(directory)
    print(path.name)
    return


######
# Prompt window by PySimpleGUI
####################################
description = "Example: 2021,7,17,6,16 Minimum 3 numbers - year, month, day. " \
              "Pattern: year, month, day, hour, minute (without 0 at beginning)."

layout = [
    [sg.Text('Input path', ), sg.InputText(size=(67, 0)), sg.FolderBrowse()],
    [
        sg.Checkbox("Basic",
                    tooltip=f"Scans for 1 level directory only (not 0), size is recursive, filtering disabled",
                    default=True)
    ],
    [
        # Name and Name Filter
        sg.Checkbox(f"{column_name}", size=(12, 1),
                    tooltip=f"Uploads '{column_name}' into end .csv file, can be filtered.",
                    default=True),
        sg.Combo([], size=(17, 1), tooltip=f"Filter for '{column_name}'"),

        # Size and Size Filter
        sg.Checkbox(f"{column_size_b}", size=(9, 1), tooltip=f"Uploads property '{column_size_b}' into end .csv file, "
                                                             f"can be filtered.",
                    default=False),
        sg.Combo([], size=(9, 1), tooltip=f"Filter for '{column_size_b}', value and above (>=)"),

        # Size_MB and Size_MB Filter
        sg.Checkbox(f"{column_size_mb}", tooltip=f"Uploads property '{column_size_mb}' into end .csv file, can be "
                                                 f"filtered.", default=True),
        sg.Combo([], size=(11, 1), tooltip=f"Filter for '{column_size_mb}', value and above (>=)"),

    ],
    [  # Extension nad Extension Filter
        sg.Checkbox(f"{column_extension}", size=(12, 1), tooltip=f"Uploads property '{column_extension}' into end "
                                                                 f".csv file, can be filtered.",
                    default=False),
        sg.Combo([], size=(17, 1), tooltip=f"Filter for '{column_extension}'")
    ],
    [  # "CreationTime" ant "CreationTime" Filter
        sg.Checkbox(
            f"{column_ct}", size=(12, 0),
            tooltip=f"Uploads property '{column_ct}' into end .csv file, can be filtered.", default=False),
        # # Calendar Button
        # sg.CalendarButton("Filter", key='CT', close_when_date_chosen=True, target='CT', no_titlebar=True),
        sg.Combo([], size=(17, 1), tooltip=f"Filter for '{column_ct}'. {description}"),
        sg.Checkbox("Before/After", tooltip="Unchecked - before date, checked - after. Works only if filter not empty"),
    ],
    [  # LastAccessTime and LastAccessTime Filter
        sg.Checkbox(
            f"{column_lat}", size=(12, 0),
            tooltip=f"Uploads property '{column_lat}' into end .csv file, can be filtered.", default=False),
        sg.Combo([], size=(17, 1), tooltip=f"Filter for '{column_lat}'. {description}"),
        sg.Checkbox("Before/After", tooltip="Unchecked - before date, checked - after. Works only if filter not empty"),
    ],
    [  # LastWriteTime and # LastWriteTime Filter
        sg.Checkbox(
            f"{column_lwt}", size=(12, 0),
            tooltip=f"Uploads property '{column_lwt}' into end .csv file, can be filtered.",
            default=False),
        sg.Combo([], size=(17, 1), tooltip=f"Filter for '{column_lwt}'. {description}"),
        sg.Checkbox("Before/After", tooltip="Unchecked - before date, checked - after. Works only if filter not empty"),

    ],
    {
        sg.Checkbox(f"{column_owner}",
                    tooltip=f"Uploads property '{column_owner}' into end .csv file, can be filtered.",
                    default=False),
    },
    [sg.Output(size=(97, 17), echo_stdout_stderr=TESTING)],
    [sg.Checkbox("Directories", tooltip="Scans directories too, besides files, directories will have size 0",
                 default=False)],
    [sg.Submit(), sg.Cancel()],
]
window = sg.Window('Get-FDInfo', layout)
##########################################
while not EXIT:
    # os.system("clear")
    event, answer = window.read()

    if TESTING:
        print(answer)

    answer_browse = answer[0]
    answer_checkbox_basic = answer[1]
    answer_checkbox_name = answer[2]
    answer_string_name = answer[3]
    answer_checkbox_size_b = answer[4]
    answer_string_size_b = answer[5]
    answer_checkbox_size_mb = answer[6]
    answer_string_size_mb = answer[7]
    answer_checkbox_extension = answer[8]
    answer_string_extension = answer[9]
    answer_checkbox_ct = answer[10]
    answer_string_ct = answer[11]
    answer_b_a_ct = answer[12]
    answer_checkbox_lat = answer[13]
    answer_string_lat = answer[14]
    answer_b_a_lat = answer[15]
    answer_checkbox_lwt = answer[16]
    answer_string_lwt = answer[17]
    answer_b_a_lwt = answer[18]
    answer_owner = answer[19]
    answer_directory = answer[20]

    if answer_browse == "":
        answer_browse = SCRIPT_ROOT
    if event == "Submit":
        # print при отсутствии переменной event
        # print(answer_basic[0])

        # print с event
        print(f"PATH: {answer_browse}\n")

        request = answer_browse
        root = Path(request)
        files_list = []
        if answer_checkbox_basic:
            for r in root.iterdir():
                size = 0
                for file in r.glob('**/*'):
                    # Добавлено исключение, когда программа не может получить доступ к свойствам файла/папки
                    # Часто это происходит из-за отсутствия прав на чтение, однако есть системные объекты,
                    # доступ на чтение которых есть только у SYSTEM(Windows), поэтому сделано так.
                    try:
                        size += file.stat().st_size
                    except Exception:
                        pass
                    # столкнулся с проблемой WindowsError(2), когда происходит сбой при обработке битого файла в
                    # windows в качестве обходного решения подходит конструкция try-except. Но лучше избавиться от
                    # таких файлов.
                    # try:
                    #    size += file.stat().st_size
                    # except WindowsError:
                    #    pass
                sb_main(r)

        elif not answer_checkbox_basic:
            root = Path(answer_browse)
            try:
                for file in root.glob('**/*'):
                    # if filter_is_file(file) is None:
                    #     pass
                    # else:
                    #     f=1
                    # filter_is_file(file)

                    filter_main(file)
            except Exception:
                pass

        result = DataFrame(files_list)
        ##########################################
        ######
        # Проверка или создание каталога Report
        ####################################
        reports = Path()
        reports = reports.joinpath(SCRIPT_ROOT, "Reports")
        if not reports.exists():
            reports.mkdir()
        ##########################################
        ######
        # csv file name for output
        ####################################
        file_csv = Path()
        # Вывод каталога в котором находится программа
        file_csv = file_csv.joinpath(reports, dt.now().isoformat().replace(":", "`").replace(".", "`") + ".csv")
        ##########################################
        ######
        # .csv file making
        ####################################
        result.to_csv(file_csv, encoding="UTF-16", sep="\t", index=0)
        print(f"\nResult file saved into: {file_csv}")
        ##########################################
    else:
        EXIT = True
        break
