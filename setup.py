from setuptools import setup

with open('get-fdinfo-linux/README.md') as f:
    long_description = f.read()

setup(
    name='Get-PSInfo-Linux-Demo',
    packages=['get-fdinfo-linux-demo'],
    version='1.3.0',
    license='Demo',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Seven-Strings',
    author_email='masamunekim@yandex.ru',
    keywords=['Linux', 'File Audit', 'Demo'],
    classifiers=[
        'Development Status :: 5 - Stable',
        'Intended Audience :: Demo',
        'Topic :: Software Development :: OS Tools',
        'License :: Demo',
        'Programming Language :: Python :: 3.9.5',
    ],
)
